//player control
var player = {
	// "constants"
	STATE_STOPPED: 0,
	STATE_RUNNING: 1,
	STATE_PAUSED: 2,

	// player "state"
	state: null,
	words: [],
	lastWord: 0,
	flashInterval: null,
	readInterval: 0,
	readSpeed: 0,
	menuClosed: false,
	
	// player controls
	wordBox: null,
	playBtn: null,
	stopBtn: null,
	speedRange: null,
	fontRadio: null,
	menuHidden: null,
	menuVisible: null,
	
	// helper functions
	loadWords: function loadWords() {
		player.words = player.wordBox.val().split(/\s/);
		player.setReadDuration();
	},
		
	showWords: function showWords() {
		if (player.lastWord < player.words.length) {
			$("#sr_speedReadWords").html(player.words[player.lastWord]);
			player.lastWord++
		} else {
			player.stop();
		}
	},
	
	setFont: function setFont() {
		var readFont = $("input[name=sr_fontControl]:checked").val();
		$("#sr_speedReadWords").css({"font-family": "\"" + readFont + "\""});
	},

	setReadInterval: function setReadInterval() {
		player.readSpeed = player.speedRange.slider("value");
		
		player.readInterval = (60 * 1000) / player.readSpeed;
		
		$("#sr_readSpeedActualWPM").html(player.readSpeed);
		$("#sr_readSpeedActualInterval").html(Math.round(player.readInterval));
		
		player.setReadDuration();
		
		if (player.state == player.STATE_RUNNING) {
			player.pause();
			player.play();
		}
	},
	
	setReadDuration: function setReadDuration() {
		var duration = Math.round(player.words.length / player.readSpeed);
		var durationText = "";
		
		if (duration < 1) {
			durationText = "under a minute";
		} else {
			durationText = "approx. " + duration + " minutes.";
		}
		
		$("#sr_readDuration").html(durationText);
	},
	
	toggleMenuVisibility: function toggleMenuVisibility() {
		if (player.menuClosed === true) {
			player.menuHidden.hide("slide", {}, 500, function() {
				player.menuVisible.show("slide", {}, 500);
			});
		} else {
			player.menuVisible.hide("slide", {}, 500, function() {
				player.menuHidden.show("slide", {}, 500);
			});
		}
		player.menuClosed = !player.menuClosed;
	},
	
	centreText: function centreText() {
		var vPortHeight = $(".controlMenu.showMe").height();
		$("#sr_readPort").css({height: vPortHeight + "px"});
	},
	
	// playback functions
	play: 	function play() {
		// set state
		if (player.state == player.STATE_STOPPED) {
			player.loadWords();
		}

		player.state = player.STATE_RUNNING;

		// control playback
		player.flashInterval = window.setInterval(player.showWords, player.readInterval);

		// (un)bind events
		player.playBtn.unbind("click", player.play);
		player.playBtn.bind("click", player.pause);

		// manipulate UI
		player.playBtn.text("Pause");
		player.stopBtn.button("enable");
		
		player.toggleMenuVisibility();
	},
	pause: 	function pause() {
		// set state
		player.state = player.STATE_PAUSED;
		
		// control playback
		window.clearInterval(player.flashInterval);
		
		// (un)bind events
		player.playBtn.unbind("click", player.pause);
		player.playBtn.bind("click", player.play);
		
		// manipulate UI
		player.playBtn.text("Play");
		player.stopBtn.button("enable");
	},
	stop: function stop() {
		// set state
		player.state = player.STATE_STOPPED;
		player.lastWord = 0;
		
		// control playback
		window.clearInterval(player.flashInterval);

		// (un)bind events
		player.playBtn.unbind("click", player.pause);
		player.playBtn.bind("click", player.play);
		
		// manipulate UI
		$("#sr_speedReadWords").html("The End.");
		player.playBtn.text("Play");
		player.stopBtn.button("disable");
	},

	// initialisation function
	__init: function init() {
		player.state = player.STATE_STOPPED;

		player.menuHidden = $(".hideMe");
		player.menuVisible = $(".showMe");
		player.menuHidden.hide();
		$(".toggleMenuVisibility").on("click", player.toggleMenuVisibility);
		
		$(window).on("resize", function(event){
			player.centreText();
		});
		
		player.centreText();
		
		player.wordBox = $("#toread");
		player.wordBox.on("change", player.loadWords);
		
		player.playBtn = $("#sr_playPauseControl");
		player.playBtn.bind("click", player.play);

		player.stopBtn = $("#sr_stopControl");
		player.stopBtn.bind("click", player.stop);
		player.stopBtn.button().button("disable");
		
		player.speedRange = $("#sr_speedControl");
		player.speedRange.slider({
			value: 250,
			min: 100,
			max: 1000,
			step: 50,
		});
		player.speedRange.on("slidechange", function(event, ui) {
				player.setReadInterval();
			});
		
		player.fontRadio = $("input[name=sr_fontControl]");
		player.fontRadio.on("change", player.setFont);
		player.fontRadio.checkboxradio({
			icon: false,
		});

		$("fieldset").controlgroup();

		
		player.setReadInterval();
		player.setFont();
	}
};


$(document).ready(player.__init);
